package TP2019.tema5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Stream;

public class MonitoredData {

	private String startTime;
	private String endTime;
	private String activity;

	public MonitoredData(String startTime, String endTime, String activity) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public MonitoredData() {

	}

	public List<MonitoredData> citesteDatele() {
		List<MonitoredData> lista = new ArrayList<MonitoredData>();
		String file = "tema5.txt";
		try (Stream<String> stream = Files.lines(Paths.get(file))) {
			stream.forEach(entry -> {
				StringTokenizer string = new StringTokenizer(entry, "		");
				while (string.hasMoreElements()) {
					startTime = string.nextElement().toString();
					endTime = string.nextElement().toString();
					activity = string.nextElement().toString();
					lista.add(new MonitoredData(startTime, endTime, activity));
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}

		return lista;

	}

	Date start, end;

	public long durataActivitate() {
		try {
			SimpleDateFormat formatter3 = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			start = formatter3.parse(startTime);
		} catch (java.text.ParseException e) {

			e.printStackTrace();
		}
		try {
			SimpleDateFormat formatter4 = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			end = formatter4.parse(endTime);
		} catch (java.text.ParseException e2) {

			e2.printStackTrace();
		}
		long diferenta = end.getTime() - start.getTime();
		return diferenta;
	}

	public int dataF(MonitoredData data) {
		String inceput = data.getStartTime();
		String sfarsit = data.getEndTime();
		StringTokenizer stElement = new StringTokenizer(inceput, "- :");
		StringTokenizer endElement = new StringTokenizer(sfarsit, "- :");
		stElement.nextElement();
		endElement.nextElement();

		stElement.nextElement();
		endElement.nextElement();

		stElement.nextElement();
		endElement.nextElement();

		int ora = Integer.parseInt((String) endElement.nextElement());
		int min = Integer.parseInt((String) endElement.nextElement());
		int sec = Integer.parseInt((String) endElement.nextElement());

		ora = ora - Integer.parseInt((String) stElement.nextElement());
		min = min - Integer.parseInt((String) stElement.nextElement());
		if (min < 0) {
			ora--;
			min = min + 60;
		}
		sec = sec - Integer.parseInt((String) stElement.nextElement());
		if (sec < 0) {
			min--;
			sec = sec + 60;
		}
		return ora * 60 + min * 60 + sec;

	}


}
