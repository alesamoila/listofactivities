package TP2019.tema5;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.*;
import java.time.Duration;
import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.*;

public class Main {

	private static DateTimeFormatter DateTimeformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	private static DateFormat Simpleformat = new SimpleDateFormat("dd HH:mm:ss");

	public static void main(String[] args) {

		MonitoredData monitored = new MonitoredData();

		////////////////////////////////////////////////////////////
		System.out.println("1.Creare lista si afisare: ");
		List<MonitoredData> lista = monitored.citesteDatele();

		for (MonitoredData data : lista) {
			System.out.println(data.getStartTime() + " " + data.getEndTime() + " " + data.getActivity());
		}
		System.out.println();

		///////////////////////////////////////////////////////////
		System.out.print("2. Numarul de zile distincte : ");
		long zileDistincte;
		zileDistincte = lista.stream().map(p -> LocalDate.parse(p.getStartTime(), DateTimeformatter)).distinct()
				.count();
		System.out.println(zileDistincte);
		System.out.println();

		////////////////////////////////////////////////////////////
		System.out.println("3. Numarul de aparitii al fiecarei activitati pe toata perioada : ");
		Map<String, Integer> mapActivitati = lista.stream()
				.collect(Collectors.toMap(p -> p.getActivity(), p -> 1, Integer::sum));

		System.out.println(mapActivitati);

		System.out.println();
		///////////////////////////////////////////////////////////////

		System.out.println("4. Numarul de aparitii al fiecarei activitati din fiecare zi : ");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		Map<Integer, Map<String, Long>> activitati_zi = lista.stream()
				.collect(Collectors.groupingBy(p -> LocalDate.parse(p.getStartTime(), formatter).getDayOfYear(),
						Collectors.groupingBy(p -> p.getActivity(), Collectors.counting())));
		System.out.println(activitati_zi);

		System.out.println();

		///////////////////////////////////////////
		System.out.println("5.Durata pentru fiecare linie : ");

		List<Integer> durata = lista.stream().map(p -> monitored.dataF(p)).collect(Collectors.toList());

		System.out.println(durata);
		System.out.println();

		/////////////////////////////////////////////////
		System.out.println("6. Durata totala a fiecarei activitati : ");
		/*
		 * Map<String, Long> durataT = lista.stream()
		 * .collect(Collectors.groupingBy(MonitoredData::getActivity,
		 * Collectors.summingLong(MonitoredData::activityDuration)));
		 */

		Map<String, Integer> durataMap = lista.stream().collect(
				Collectors.groupingBy(MonitoredData::getActivity, Collectors.summingInt(a -> monitored.dataF(a))));

		System.out.println(durataMap);
		System.out.println();

		//////////////////////////////////////////////////
		System.out.println("7. Afiseaza doar activitatile care in 90% din cazuri au durat mai putin de 5 minute : ");
		Map<String, Integer> toateActivitatile = lista.stream()
				.collect(Collectors.toMap(p -> p.getActivity(), p -> 1, Integer::sum));
		// System.out.println(allActivities);
		Map<String, Integer> AcSub5Minute = lista.stream().filter(p -> p.durataActivitate() < 300000)
				.collect(Collectors.toMap(p -> p.getActivity(), p -> 1, Integer::sum));
		// System.out.println(allActivitiesUnderFive);
		List<String> rez = AcSub5Minute.entrySet().stream()
				.filter(p -> p.getValue() / toateActivitatile.get(p.getKey()) * 100 >= 90).map(p -> p.getKey())
				.collect(Collectors.toList());
		System.out.println(rez);

	}

	private static Date convertDate(long valoare) {
		Date data = new Date(valoare);
		try {
			String string = Simpleformat.format(data);
			return Simpleformat.parse(string);

		} catch (ParseException err) {
			err.printStackTrace();
		}
		return null;
	}

}
